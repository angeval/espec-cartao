package com.itau.cartao.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_REQUEST,reason="Cartão já existente")
public class CartaoAlreadyExistsException extends RuntimeException {
}
