package com.itau.cartao.cartao.models.mapper;

import com.itau.cartao.cartao.models.Cartao;
import com.itau.cartao.cartao.models.dtos.*;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        //Cliente cliente = clienteClient.getById(cartaoCreateRequest.getClienteId());

        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());
        cartao.setClienteId(cartaoCreateRequest.getClienteId());

        return cartao;
    }

    public CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();
            createCartaoResponse.setId(cartao.getId());
            createCartaoResponse.setNumero(cartao.getNumero());
            createCartaoResponse.setClienteId(cartao.getClienteId());
            createCartaoResponse.setAtivo(cartao.isAtivo());
            return createCartaoResponse;
    }

    public static Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public static UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setClienteId(cartao.getClienteId());
        updateCartaoResponse.setAtivo(cartao.isAtivo());

        return updateCartaoResponse;
    }

    public static GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setClienteId(cartao.getClienteId());

        return getCartaoResponse;
    }

}
