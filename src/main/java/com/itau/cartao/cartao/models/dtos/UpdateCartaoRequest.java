package com.itau.cartao.cartao.models.dtos;

public class UpdateCartaoRequest {
    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
