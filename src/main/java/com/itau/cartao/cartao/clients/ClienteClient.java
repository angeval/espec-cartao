package com.itau.cartao.cartao.clients;

import com.itau.cartao.cartao.models.dtos.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {
        @GetMapping("/cliente/{id}")
        Cliente findById(@PathVariable int id);
}
