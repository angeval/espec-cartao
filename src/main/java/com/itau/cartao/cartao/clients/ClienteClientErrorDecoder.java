package com.itau.cartao.cartao.clients;

import com.itau.cartao.cartao.exceptions.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new ClienteNotFoundException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
