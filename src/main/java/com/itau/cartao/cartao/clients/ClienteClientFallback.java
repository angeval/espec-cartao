package com.itau.cartao.cartao.clients;

import com.itau.cartao.cartao.exceptions.ClienteNotFoundException;
import com.itau.cartao.cartao.models.dtos.Cliente;
import com.netflix.client.ClientException;

import java.io.IOException;
import java.net.ConnectException;

public class ClienteClientFallback implements ClienteClient {

    private Exception cause;

    ClienteClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public Cliente findById(int id) {
        if (cause.getLocalizedMessage() != null && cause.getLocalizedMessage().contains("ClientException")) {
            throw new RuntimeException("O serviço de cliente está fora do ar");
        }

        if (cause instanceof ConnectException || cause instanceof IOException ) {
            throw new RuntimeException("O serviço de clientes de cartões está com problemas. Tente mais tarde.");
        }

        throw (RuntimeException) cause;
        // cliente fake
        // fazer um log e disparar alguma fila para refazer o cliente quando o serviço voltasse para o ar
        //Cliente cliente = new Cliente();
        //cliente.setId(-1);
        //cliente.setName("Cliente inexistente");
        //return cliente;
    }
}
