package com.itau.cartao.cartao.services;

import com.itau.cartao.cartao.clients.ClienteClient;
import com.itau.cartao.cartao.exceptions.CartaoAlreadyExistsException;
import com.itau.cartao.cartao.exceptions.CartaoNotFoundException;
import com.itau.cartao.cartao.exceptions.ClienteNotFoundException;
import com.itau.cartao.cartao.models.Cartao;
import com.itau.cartao.cartao.models.dtos.Cliente;
import com.itau.cartao.cartao.repositories.CartaoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class CartaoService {
        @Autowired
        private CartaoRepository cartaoRepository;

        @Autowired
        private ClienteClient clienteClient;

        public Cartao criarCartao(Cartao cartao){
            Optional<Cartao> cartaoDatabase = cartaoRepository.findByNumero(cartao.getNumero());
            if(cartaoDatabase.isPresent()){
                throw new CartaoAlreadyExistsException();
            }
            //try {
                clienteClient.findById(cartao.getClienteId());
                return cartaoRepository.save(cartao);
            //} catch (FeignException.NotFound e) {
            //    throw new ClienteNotFoundException();
            //}
        }

        public Cartao buscarPorId(int id){
            Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);
            if(cartaoOptional.isPresent()) {
                return cartaoOptional.get();
            }
            throw new CartaoNotFoundException();
        }

        public Cartao buscarPorNumero(String numero){
            Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
            if(cartaoOptional.isPresent()) {
                return cartaoOptional.get();
            }
            throw new CartaoNotFoundException();
        }

        public Iterable<Cartao> buscarTodosCartoes(){
            Iterable<Cartao> cartoes = cartaoRepository.findAll();
            return cartoes;
        }

        //public Cartao ativarCartao(int id, boolean ativo){
        //    Optional<Cartao> cartaoOptional = buscarPorId(id);
        //    if (cartaoOptional.isPresent()){
        //        Cartao cartaoData = cartaoOptional.get();
        //        cartaoData.setAtivo(ativo);
        //        return cartaoRepository.save(cartaoData);
        //    }
        //    throw new org.hibernate.ObjectNotFoundException(Cartao.class, "O Cartão não foi encontrado");
        //}

        public Cartao ativarCartao(Cartao cartao){
                Cartao cartaoData = buscarPorNumero(cartao.getNumero());
                cartaoData.setAtivo(cartao.isAtivo());
                return cartaoRepository.save(cartaoData);
        }
}