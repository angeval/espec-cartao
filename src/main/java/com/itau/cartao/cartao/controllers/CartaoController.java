package com.itau.cartao.cartao.controllers;
import com.itau.cartao.cartao.models.Cartao;
import com.itau.cartao.cartao.models.dtos.*;
import com.itau.cartao.cartao.models.mapper.CartaoMapper;
import com.itau.cartao.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
    public class CartaoController {
        @Autowired
        private CartaoService cartaoService;

        @PostMapping
        @ResponseStatus(code= HttpStatus.CREATED)
        public CreateCartaoResponse criarCartao(@RequestBody @Valid CreateCartaoRequest createCartaoRequest) {
            CartaoMapper cartaoMapper = new CartaoMapper();
            Cartao cartao = cartaoMapper.fromCreateRequest(createCartaoRequest);
            cartao = cartaoService.criarCartao(cartao);
            return cartaoMapper.toCreateResponse(cartao);
        }

        @GetMapping
        public ResponseEntity<Iterable> buscarTodosCartoes(){
            Iterable<Cartao>  cartaoIterable = cartaoService.buscarTodosCartoes();
            return ResponseEntity.status(200).body(cartaoIterable);

        }

        @GetMapping("/id/{id}")
        public GetCartaoResponse buscarCartaoID(@PathVariable int id){
            Cartao cartao = cartaoService.buscarPorId(id);
            return CartaoMapper.toGetResponse(cartao);
        }

        @GetMapping("/{numeroCartao}")
        public GetCartaoResponse buscarCartaoID(@PathVariable String numeroCartao){
            Cartao cartao = cartaoService.buscarPorNumero(numeroCartao);
            return CartaoMapper.toGetResponse(cartao);
        }

        //@PatchMapping("/{id}")
        //public Cartao ativarCartao(@PathVariable Integer id, @RequestBody @Valid Cartao cartao){
        //        return cartaoService.ativarCartao(id, cartao.isAtivo());
        //}

        @PatchMapping("/{numero}")
        public UpdateCartaoResponse ativarCartao(@PathVariable String numero,
                                                 @RequestBody @Valid UpdateCartaoRequest updateCartaoRequest){
            Cartao cartao =  CartaoMapper.fromUpdateRequest(updateCartaoRequest);
            cartao.setNumero(numero);
            cartao = cartaoService.ativarCartao(cartao);
            return CartaoMapper.toUpdateResponse(cartao);

        }
    }
