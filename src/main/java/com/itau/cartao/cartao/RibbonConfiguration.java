package com.itau.cartao.cartao;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import com.netflix.loadbalancer.WeightedResponseTimeRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

        @Bean
        public IRule iRule() {
            //aleatório
            return new RandomRule();

            //uma vez em cada máquina
            //return new RoundRobinRule();

            //por balanceamento de menor peso
            //return new WeightedResponseTimeRule();

        }

    }